const express = require("express");
const app = express();
var PORT = process.env.PORT || 5000;
const bodyParser = require("body-parser");
var fileUpload = require('express-fileupload');
var querystring = require('querystring');
var session = require('express-session')
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(fileUpload());

const mongodb = require("mongodb");
const mongoCLient = mongodb.MongoClient;
const ObjectId = mongodb.ObjectId;

const http = require("http").createServer(app);
const bcrypt = require("bcrypt");

var jwt = require("jsonwebtoken");
var accessTokenSecret = "appSecretToken123456";

app.use(session({
	secret: 'appSecretToken123456',
	resave: false,
	saveUninitialized: true,
}))
app.use("/public", express.static(__dirname + "/public"));
app.set("view engine", "pug");

var socketIO = require("socket.io")(http);
var socketID = "";
var users = [];


socketIO.on("connection", function (socket) {
	console.log("User connected", socket.id);
	socketID = socket.id;
})


const MongoClient = require('mongodb').MongoClient;
const uri = "mongodb+srv://admin:admin@cluster0.xl7mg.mongodb.net/test?retryWrites=true&w=majority";


http.listen(PORT, function () {
	console.log("Server started");
	mongoCLient.connect(uri, { useNewUrlParser: true }, function (err, client) {
		if (err) {
			console.log("Error in connecting database");
		}
		var db = client.db("chatapp");

		app.get("/", AlreadyLoggedIn, function (req, res) {
			res.render("login")
		})

		app.get('/logout', isLoggedIn, function (req, res) {
			try {
				db.collection("users").findOneAndUpdate({
					"email": req.session.user.email
				}, {
						$set: {
							"status": "offline"
						}
					}).then(() => {
						socketIO.emit("userLogout", {
							"from": req.session.user
						});
						req.session.destroy(function () {
							console.log("user logged out")
							res.redirect('/');
						});
					})
			}
			catch (err) {
				console.log("-----------error in log out------------");
				console.log(err);
			}
		});

		app.post("/", AlreadyLoggedIn, function (req, res) {
			var parsed = querystring.parse(req.body.formDetails);
			let email = parsed.email;
			let pwd = parsed.password;
			try {
				db.collection("users").findOne({
					"email": email
				}, function (err, user) {
					if (user == null) {
						let ajaxRes = {
							"status": "Failed",
							"msg": "Email does not exits"
						}
						res.send(ajaxRes)
					}
					else {
						bcrypt.compare(pwd, user.password, function (err, isVerify) {
							if (isVerify) {
								var accessToken = jwt.sign({ email: email }, accessTokenSecret);
								db.collection("users").findOneAndUpdate({
									"email": email
								}, {
										$set: {
											"status": "online"
										}
									}, function (err, data) {
										req.session.user = user;
										socketIO.emit("userOnline", {
											"from": user
										})
										let ajaxRes = {
											"status": "Success",
											"msg": "Login success",
											"user": user
										}
										res.send(ajaxRes);
									})
							}
							else {
								let ajaxRes = {
									"status": "Failed",
									"msg": "Incorrect password",
								}
								res.send(ajaxRes);
							}
						})
					}
				})
			}
			catch (err) {
				console.log("-----------error in login------------");
				console.log(err);
				let ajaxRes = {
					"status": "Failed",
					"msg": "Cannot login",
				}
				res.send(ajaxRes);
			}
		})

		app.get("/signup", AlreadyLoggedIn, function (req, res) {
			res.render("signup");
		})

		app.post("/signup", AlreadyLoggedIn, function (req, res) {
			var parsed = querystring.parse(req.body.formDetails);
			let name = parsed.fullName;
			let userName = parsed.userName;
			let email = parsed.email;
			let pwd = parsed.password;
			try {
				db.collection('users').findOne({
					$or: [{
						"email": email
					}, {
						"userName": userName
					}]
				}, (error, user) => {
					if (user == null) {
						bcrypt.hash(pwd, 10, function (err, hash) {
							db.collection("users").insertOne({
								"name": name,
								"userName": userName,
								"email": email,
								"password": hash,
								"friends": [],
								"status": ''
							}, function (err, data) {
								let ajaxRes = {
									"status": "Success",
									"msg": "Signed Up Successfully, login now"
								}
								res.send(ajaxRes)
							})
						})
					}
					else {
						let ajaxRes = {
							"status": "Failed",
							"msg": "Username or Email alreday registered"
						}
						res.send(ajaxRes)
					}
				})
			}
			catch (err) {
				console.log("-----------error in signup------------");
				console.log(err);
				let ajaxRes = {
					"status": "Failed",
					"msg": "Cannot signUp"
				}
				res.send(ajaxRes)
			}
		})

		app.get("/usersList", isLoggedIn, function (req, res) {
			try {
				db.collection("users").find({ 'status': 'online', 'email': { $ne: req.session.user.email } }).toArray((err, onlineUsers) => {
					res.render("chat/chaton", {
						url: req.path,
						userList: onlineUsers,
						logUser: req.session.user
					})
				});
			}
			catch (err) {
				console.log("-----------error in getUsersList------------");
				console.log(err);
				res.render("chat/chaton", {
					url: req.path,
					userList: '',
					logUser: req.session.user
				})
			}
		})

		app.post("/usersList", isLoggedIn, function (req, res) {
			try {
				db.collection("users").findOne({
					"email": req.session.user.email
				}, function (err, user) {
					var index = user.friends.findIndex(function (friend) {
						return friend._id == req.body.selectedId
					})
					var inbox = user.friends[index].inbox;
					let ajaxRes = {
						"status": "Success",
						"msg": "Chat has been fetched",
						"data": inbox,
						"userId": req.session.user._id
					}
					res.send(ajaxRes)
				})
			}
			catch (err) {
				console.log("-----------error in postUsersList------------");
				console.log(err);
				let ajaxRes = {
					"status": "Failed",
					"msg": "Cannot fetch user chat",
					"data": inbox,
					"userId": req.session.user._id
				}
				res.send(ajaxRes)
			}
		})

		app.post("/getUserFriends", isLoggedIn, function (req, res, next) {
			try {
				db.collection("users").findOne({
					"email": req.session.user.email
				}, function (err, user) {
					let ajaxRes = {
						"status": "Success",
						"msg": "User updated list",
						"userUpdated": user
					}
					res.send(ajaxRes);
				})
			}
			catch (err) {
				console.log("-----------error in getUserFriends------------");
				console.log(err);
			}
		})

		app.post("/sendRequest", isLoggedIn, function (req, res, next) {
			async function sendRequest() {
				try {
					var userFrn = await db.collection("users").findOne({
						$and: [{
							"email": req.session.user.email
						}, {
							"friends._id": ObjectId(req.body.selectedId)
						}]
					})
					if (userFrn == null) {
						var user = await db.collection("users").findOne({
							"email": req.session.user.email
						})

						var friend = await db.collection("users").findOne({
							"_id": ObjectId(req.body.selectedId)
						})


						if (friend == null) {
							let ajaxRes = {
								"status": "Failed",
								"msg": "Failed,Request has not been sent"
							}
							res.send(ajaxRes)
						}
						else {
							var data = await db.collection("users").updateOne({
								"_id": ObjectId(req.body.selectedId)
							}, {
									$push: {
										"friends": {
											"_id": user._id,
											"name": user.userName,
											"status": "Requested",
											"sentbyme": "false",
											"inbox": []
										}
									}
								})

							var final = await db.collection("users").updateOne({
								"_id": user._id
							}, {
									$push: {
										"friends": {
											"_id": friend._id,
											"name": friend.userName,
											"status": "Pending",
											"sentbyme": "true",
											"inbox": []
										}
									}
								}, function (err, finalUpdate) {
									socketIO.to(users[friend._id]).emit("requestReceived", {
										"from": user._id
									})
								})
               return 'Success';
						}
					}
					else {
						let ajaxRes = {
							"status": "Failed",
							"msg": "Try Reload Page"
						}
						res.send(ajaxRes)
					}
				}
				catch (err) {
					console.log("-----------error in sendrequest------------");
					console.log(err);
					let ajaxRes = {
						"status": "Failed",
						"msg": "Failed,Request has not been sent"
					}
					res.send(ajaxRes)
				}
			}
			sendRequest().then((result) => {
				console.log(result);
				console.log("---------send request updated---------------");
				let ajaxRes = {
					"status": "Success",
					"msg": "Request has been sent"
				}
				res.send(ajaxRes)
			})
		})

		app.post("/acceptRequest", isLoggedIn, function (req, res, next) {
			async function acceptRequest(){
				try{
					var user = await 	db.collection("users").findOne({
						"email": req.session.user.email
					});
					var friend = await db.collection("users").findOne({
						"_id": ObjectId(req.body.selectedId)
					});
					if (friend == null) {
						console.log("----------------frined does not exist-----------");
						let dataReturn = {
							status : 'Failed'
						}
						return dataReturn;
					}
					else {
						var data = await 	db.collection("users").updateOne({
							$and: [{
								"_id": ObjectId(req.body.selectedId)
							}, {
								"friends._id": user._id
							}]
						}, {
								$set: {
									"friends.$.status": "Accepted"
								}
							})
						var finalData = await db.collection("users").updateOne({
							$and: [{
								"_id": user._id
							}, {
								"friends._id": ObjectId(req.body.selectedId)
							}]
						}, {
								$set: {
									"friends.$.status": "Accepted"
								}
							}, function (err, final) {
								console.log("---------req accepted---------------");
								socketIO.to(users[friend._id]).emit("requestAccepted", {
									"from": user._id
								})
							})
						let dataReturn = {
							status : 'Success'
						}
						return dataReturn;
					}
				}
				catch(err){
					console.log("-----------error in accept request------------");
					console.log(err);
					let ajaxRes = {
						"status": "Failed",
						"msg": "Failed, AcceptRequest is not success"
					}
					res.send(ajaxRes)
				}
			}
			acceptRequest().then((result)=>{
				console.log(result.status);
				if(result && result.status == 'Success'){
					let ajaxRes = {
						"status": "Success",
						"msg": "AcceptRequest is success"
					}
					res.send(ajaxRes)
				}
				else{
					let ajaxRes = {
						"status": "Failed",
						"msg": "AcceptRequest is not success"
					}
					res.send(ajaxRes)
				}
			})
		})

		app.post("/sendMessage", isLoggedIn, function (req, res, next) {
			try {
				db.collection("users").findOne({
					"email": req.session.user.email
				}, function (err, user) {
					db.collection("users").findOne({
						"_id": ObjectId(req.body.selectedId)
					}, function (err, friend) {
						if (friend == null) {
							console.log("----------------frined does not exist-----------");
							let ajaxRes = {
								"status": "Failed",
								"msg": "Message is not sent, user does not exists"
							}
							res.send(ajaxRes)
						}
						else {
							db.collection("users").updateOne({
								$and: [{
									"_id": ObjectId(req.body.selectedId)
								}, {
									"friends._id": user._id
								}]
							}, {
									$push: {
										"friends.$.inbox": {
											"_id": ObjectId(),
											"message": req.body.message,
											"from": user._id
										}
									}
								}, function (err, data) {
									db.collection("users").updateOne({
										$and: [{
											"_id": user._id
										}, {
											"friends._id": ObjectId(req.body.selectedId)
										}]
									}, {
											$push: {
												"friends.$.inbox": {
													"_id": ObjectId(),
													"message": req.body.message,
													"from": user._id
												}
											}
										}, function (err, final) {
											socketIO.to(users[friend._id]).emit("messageReceived", {
												"message": req.body.message,
												"from": user._id
											})
											let ajaxRes = {
												"status": "Success",
												"msg": "Message has been sent"
											}
											res.send(ajaxRes)
										})
								})
						}
					})
				})
			}
			catch (err) {
				console.log("-----------error in sendMessage------------");
				console.log(err);
				let ajaxRes = {
					"status": "Failed",
					"msg": "Message is not sent, user does not exists"
				}
				res.send(ajaxRes)
			}
		})

		app.post("/connectSocket", isLoggedIn, function (req, res, next) {
			try {
				db.collection("users").findOne({
					"email": req.session.user.email
				}, function (err, user) {
					if (user == null) {
						console.log("-------connect socket no user---------");
						let ajaxRes = {
							"status": "Failed",
							"message": "Socket Not Connected, try login again"
						}
						res.send(ajaxRes);
					} else {
						console.log("socketID of user:", user.email, socketID);
						users[user._id] = socketID;
						let ajaxRes = {
							"status": "Success",
							"message": "Socket has been connected"
						}
						res.send(ajaxRes);
					}
				})
			}
			catch (err) {
				console.log("-----------error in connectSocket------------");
				console.log(err);
				let ajaxRes = {
					"status": "Failed",
					"message": "Socket Not Connected, try login again"
				}
				res.send(ajaxRes);
			}
		})

		function isLoggedIn(req, res, next) {
			if (req.session.user) {
				return next();
			}
			res.redirect('/');
		}

		function AlreadyLoggedIn(req, res, next) {
			if (req.session.user) {
				res.redirect('/usersList');
				return;
			}
			return next();
		}
	})
})